﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PRBD_Framework;

namespace prbd_1617_G04
{
    public class Tools
    {

        public static int getNbrPlacesReservation(Show show, Client client,int IdCat  ) {
            
            if (show != null && client!=null)
            {
                var q = from m in App.Model.Reservation
                        where m.NumS == show.IdS && m.NumC==client.IdC && m.NumCat == IdCat
                        select m;
                var res = q.FirstOrDefault();
                if(res!=null)
                 return res.Nbr;
                return 0;
            }
            return 0;
        }
        public static void setNbrPlacesReservation(Show show, Client client, int idCat, int val) {
            
            if (show != null && client!=null && val>0)
            {
                var q = from m in App.Model.Reservation
                        where m.NumCat == idCat && m.NumC == client.IdC && m.NumS ==show.IdS
                        select m;
                var res = q.FirstOrDefault();
                if (res != null)
                    res.Nbr = val;
                else {
                    Reservation reser = new Reservation();
                    reser.Category = getCategory(idCat);
                    reser.Client = client;
                    reser.Show = show;
                    reser.Nbr = val;
                    show.Reservation.Add(reser);
                    client.Reservation.Add(res);
                    getCategory(idCat).Reservation.Add(reser);          
                }
            }
        }
        public static Category getCategory(int a) {
            var q = from m in App.Model.Category
                    where m.IdCat == a
            select m;
            return q.FirstOrDefault();
        }
        public static Category getCategory(string cat)
        {
            var q = from m in App.Model.Category
                    where m.CatName == cat
                    select m;
            Category res = q.FirstOrDefault();
            
            return res;
        }
        public static int getId(string  cat) {
            if (cat == "CatA")
                return 1;
            else if (cat == "CatB")
                return 2;
            else
                return 3;
        }
      
        public static string getPlaceRest(string cat, Show s)
        {
            if (s != null)
            {
                var q = from m in s.Reservation
                        where m.Category.CatName == cat
                        select m.Nbr;
                var res = q.Sum();
                if (cat == "CatA")
                {
                    res = 50 - res;
                    return "" + res;
                }
                else if (cat== "CatB")
                {
                    res = 100 - res;
                    return "" + res;
                }

                else
                    return "" + (200 - res);
            }
            return "";
        }
        public static int PlaceRest(string cat, Show s)
        {
            var q = from m in s.Reservation
                    where m.Category.CatName == cat
                    select m.Nbr;
            var res = q.Sum();
            if (cat == "CatA")
            {
                res = 50 - res;
                return res;
            }
            else if (cat == "CatB")
            {
                res = 100 - res;
                return res;
            }

            else
                return (200 - res);
        }
        public static void setPrice(Show show, string cat, decimal val)
        {
            if (show != null)
            {
                var q = from m in show.PriceList
                        where m.Category.CatName == cat
                        select m;
                var res = q.FirstOrDefault();
                if (res != null)
                {
                    res.Price = val;

                }
                else
                {
                    PriceList price = new PriceList();
                    price.Price = val;
                    price.Show = show;
                    price.Category = getCategory(cat);
                    show.PriceList.Add(price);

                }
            }
            return;
        }
        public static decimal getPrice(Show Show,string cat)
        {
            if (Show != null) {
                var q = from m in Show.PriceList
                        where m.Category.CatName == cat
                        select m;
                var res = q.FirstOrDefault();
                if (res == null)
                    return 0;
                else
                    return res.Price;
            }
            return 0;    
        }
    
        public static void deleteReservation(Show show, Client client, int idcat)
        {
            if (show != null && client != null)
            {
                var q = from m in App.Model.Reservation
                        where m.NumS == show.IdS && m.NumC == client.IdC && m.NumCat == idcat
                        select m;
                var res = q.FirstOrDefault();
                if (res != null)
                    App.Model.Reservation.Remove(res);
                   

            }
        }
     
      

    }
}
