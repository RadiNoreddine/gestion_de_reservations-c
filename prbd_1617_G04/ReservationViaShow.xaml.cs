﻿using PRBD_Framework;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace prbd_1617_G04
{
    /// <summary>
    /// Logique d'interaction pour ReservationDetailView.xaml
    /// </summary>
    public partial class ReservationViaShow : UserControlBase
    {
        private static User vendor = App.Model.User.Find(2);
        public ICommand Save { get; set; }
        public ICommand Cancel { get; set; }
        public ICommand ClearFilter { get; set; }
        public ICommand delete { get; set; }
        public Show Show { get; set; }
        public Client Client { get; set; }
        public Reservation Reservation { get; set; }


        private ObservableCollection<Client> clients;

        public ReservationViaShow(Show show)
        {
            InitializeComponent();
            if (DesignerProperties.GetIsInDesignMode(this))
                return;
            DataContext = this;
            Clients = new ObservableCollection<Client>(App.Model.Client);
            ClearFilter = new RelayCommand(() => { Filter = ""; });
            delete = new RelayCommand(deleteAction, IsVendor);
            Save = new RelayCommand(SaveAction, CanSaveOrCancelAction);
            Cancel = new RelayCommand(CancelAction, CanSaveOrCancelAction);
            Show = show;
        }
        private bool IsVendor() {
            return vendor.Equals(App.CurrentUser);
        }
        public int cat;
        public int nomcat
        {
            get
            {
                return cat;
            }
            set
            {
                cat = value; 
                nbrplace = Tools.getNbrPlacesReservation(Show, Client, nomcat + 1);
                RaisePropertyChanged(nameof(nomcat));
                RaisePropertyChanged(nameof(NbrPlaceRestante));
                RaisePropertyChanged(nameof(PlaceTotale));
            }
        }
        public int nbr;
        public int nbrplace
        {
            get
            {
                return nbr;
            }
            set
            {
                
                nbr = value;
                RaisePropertyChanged(nameof(nbrplace));
                RaisePropertyChanged(nameof(NbrPlaceRestante));
                
               
            }
        }

        public ObservableCollection<Client> Clients
        {
            get
            {
                return clients;
            }
            set
            {
                clients = value;
                RaisePropertyChanged(nameof(Clients));
            }
        }

        private string filter;
        public string Filter
        {
            get { return filter; }
            set
            {
                filter = value;
                ApplyFilterAction();
                RaisePropertyChanged(nameof(Filter));
            }
        }
        private void ApplyFilterAction()
        {
            IEnumerable<Show> query = App.Model.Show;
            if (!string.IsNullOrEmpty(Filter))
                query = from m in App.Model.Show
                        where
                            m.ShowName.Contains(Filter)
                        select m;
            Clients = new ObservableCollection<Client>(Clients);
        }


        private void SaveAction()
        {
            Tools.setNbrPlacesReservation(Show, Client, nomcat + 1, nbrplace);
            App.Model.SaveChanges();
            RaisePropertyChanged(nameof(NbrPlaceRestante));
            App.Messenger.NotifyColleagues(App.MSG_RESERVATION_CHANGED, Reservation);
        }
        private bool CanSaveOrCancelAction()
        {
            if(IsVendor())
                return Client!= null && nbrplace > 0 && nbrplace <= NbrPlaceRestante;
            return false;
        }

        private void CancelAction()
        {
            nbrplace = Tools.getNbrPlacesReservation(Show, Client, nomcat + 1);
        }
        private void deleteAction()
        {
            Tools.deleteReservation(Show, Client, nomcat + 1);
            App.Model.SaveChanges();
            nbrplace = Tools.getNbrPlacesReservation(Show, Client, nomcat + 1);
            RaisePropertyChanged(nameof(NbrPlaceRestante));
        }

        public Client ClientSelection
        {
            get { return Client; }
            set
            {
                Client = value;
                nbrplace = Tools.getNbrPlacesReservation(Show, Client, nomcat + 1);
                RaisePropertyChanged(nameof(ClientSelection));
                RaisePropertyChanged(nameof(NbrPlaceRestante));
            }

        }

        public string ShowName
        {
            get
            {
                
                    return "Reservation for :  " + Show.ShowName;
                
            }
            set { }
        }

        public int NbrPlaceRestante
        {
            get
            {
                return placeRestante();
            }
            set { RaisePropertyChanged(nameof(NbrPlaceRestante)); }
        }
        public int placeRestante()
        {
            if (Show != null)
            {
                var q = from m in App.Model.Reservation
                        where m.NumCat == (nomcat + 1) && m.NumS == Show.IdS
                        select m.Nbr;
                if(q.FirstOrDefault()!=0)
                   return nbrTotal() - q.Sum();
                return nbrTotal();
            }
            else
                return nbrTotal();
        }


        public int nbrTotal()
        {
            if (nomcat == 0)
                return 50;
            else if (nomcat == 1)
                return 100;
            else return 200;
        }
        public int PlaceTotale
        {
            get { return nbrTotal(); }
            set { RaisePropertyChanged(nameof(PlaceTotale)); }
        }
        
        
    }
}

