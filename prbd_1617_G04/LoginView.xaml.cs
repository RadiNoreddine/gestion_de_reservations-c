﻿using PRBD_Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace prbd_1617_G04
{
    /// <summary>
    /// Logique d'interaction pour LoginView.xaml
    /// </summary>
    public partial class LoginView : WindowBase
    {
        public ICommand Login { get; set; }
        public ICommand Cancel { get; set; }

        private string pseudo;
        public string Pseudo { get { return pseudo; } set { pseudo = value; Validate(); } }

        private string password;
        public string Password { get { return password; } set { password = value; Validate(); } }

        
        public LoginView()
        {
            InitializeComponent();
            DataContext = this;
            Login = new RelayCommand(LoginAction, () => { return pseudo != null && password != null && !HasErrors; });
            Cancel = new RelayCommand(() => Close());
            ColdStart();
        }
        private new User Validate()
        {
            ClearErrors();
            var user = App.Model.User.Find(getId(Pseudo));
            if (string.IsNullOrEmpty(Pseudo))
            {
                AddError("Pseudo", Properties.Resources.Error_Required);
                Console.WriteLine("Pseudo empty");
            }
            if (Pseudo != null)
            {
                if (Pseudo.Length < 3)
                    AddError("Pseudo", Properties.Resources.Error_LengthGreaterEqual3);
                else
                {
                    if (user == null)
                        AddError("Pseudo", Properties.Resources.Error_DoesNotExist);
                }
            }
            if (string.IsNullOrEmpty(Password))
                AddError("Password", Properties.Resources.Error_Required);
            if (Password != null)
            {
                if (Password.Length < 3)
                    AddError("Password", Properties.Resources.Error_LengthGreaterEqual3);
                else if (user != null && user.Pwd != Password)
                    AddError("Password", Properties.Resources.Error_WrongPassword);
            }

            RaiseErrors();

            return user;
        }
        private int getId(string nom) {
            var q = from m in App.Model.User
                    where m.Login == nom
                    select m.IdU;
            return q.FirstOrDefault();
        }
        private void ColdStart()
        {
            App.Model.User.Find(getId("DUMMY"));
        }
        private void LoginAction()
        {
            var user = Validate();
            if (!HasErrors)
            {
                App.CurrentUser = user;
                ShowMainView();
                Close();
            }
        }
        private static void ShowMainView()
        {
            var mainView = new MainView();
            mainView.Show();
            Application.Current.MainWindow = mainView;
        }
    }
}
