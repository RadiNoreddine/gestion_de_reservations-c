﻿using PRBD_Framework;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace prbd_1617_G04
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainView : WindowBase
    {
        public ICommand Shows { get; set; }
        public ICommand Clients { get; set; }
        public ICommand Reservations { get; set; }
       



        public MainView()
        {
            InitializeComponent();
            if (DesignerProperties.GetIsInDesignMode(this))
                return;
            DataContext = this;
            Shows = new RelayCommand(() => { App.Messenger.NotifyColleagues(App.MSG_LIST_SHOW); });
            Clients = new RelayCommand(() => { App.Messenger.NotifyColleagues(App.MSG_LIST_CLIENT); });
            Reservations = new RelayCommand(() => { App.Messenger.NotifyColleagues(App.MSG_LIST_RESERVATION); });
            App.Messenger.Register(App.MSG_LIST_SHOW, () => { newTabForShows(); });
            App.Messenger.Register(App.MSG_LIST_CLIENT, () => { newTabForClients(); });
            App.Messenger.Register(App.MSG_LIST_RESERVATION, () => { newTabForResevations(); });
            App.Messenger.Register<TabItem>(App.MSG_CLOSE_TAB, tab => { tabControl.Items.Remove(tab); });
            App.Messenger.Register<Show>(App.MSG_SHOW_CHANGED, Show => { App.Model.SaveChanges(); });
            App.Messenger.Register<Show>(App.MSG_RESERVATION_SHOW, Show => { reservationViaShow(Show); });
            App.Messenger.Register<Client>(App.MSG_RESERVATION_CLIENT, Client => { reservationViaClient(Client); });
            App.Messenger.Register<Reservation>(App.MSG_DISPLAY_RESERVATION, reser => { reservationViaClient(reser); });
        }
        private void reservationViaShow(Show show)
        {
           
            var tab = new TabItem()
            {
                Header = "<new Reservation>",
                Content = new ReservationViaShow(show),
            };
            fermeOnglet(tab);
       
        }
        private void reservationViaClient(Client client) {
           
            var tab = new TabItem()
            {
                Header = "<new Reservation>",
                Content = new ReservationViaClient(client),
            };
            fermeOnglet(tab);
        }
        private void reservationViaClient(Reservation reservation)
        {
           
            var tab = new TabItem()
            {
                Header = "<new Reservation>",
                Content = new ReservationViaClient(reservation),
            };
            fermeOnglet(tab);
        }


        private void newTabForShows()
        {
            var tab = new TabItem()
            {
                Header = "<Show>",
                Content = new ShowView()
            };
            fermeOnglet(tab);


        }

        private void newTabForClients()
        {
            var tab = new TabItem()
            {
                Header = "<Clients>",
                Content = new ClientView()
            };
            fermeOnglet(tab);
        }

        private void newTabForReservation(Reservation reservation,Show show, bool isNew)
        {
            var tab = new TabItem()
            {
                Header ="<new Reservation>" ,
               Content = new ReservationViaShow(show)
            };
            fermeOnglet(tab);


        }
        private void newTabForResevations()
        {
            var tab = new TabItem()
            {
                Header = "<Reservation>",
                Content = new ReservationView()
            };
            fermeOnglet(tab);


        }
     
        public void fermeOnglet(TabItem tab)
        {
            tab.MouseDown += (o, e) =>
            {
                if (e.ChangedButton == MouseButton.Middle && e.ButtonState == MouseButtonState.Pressed)
                    tabControl.Items.Remove(o);
            };
            tab.KeyDown += (o, e) =>
            {
                if (e.Key == Key.W && Keyboard.IsKeyDown(Key.LeftCtrl))
                    tabControl.Items.Remove(o);
            };
            tabControl.Items.Add(tab);
            Dispatcher.InvokeAsync(() => tab.Focus());
        }


    }
}

