﻿using PRBD_Framework;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace prbd_1617_G04
{
    /// <summary>
    /// Logique d'interaction pour ReservationViaClient.xaml
    /// </summary>
    public partial class ReservationViaClient : UserControlBase
    {
        private static User vendor = App.Model.User.Find(2);
        public ICommand Save { get; set; }
        public ICommand Cancel { get; set; }
        public ICommand ClearFilter { get; set; }
        public ICommand delete { get; set; }
        public Show Show { get; set; }
        public Client Client { get; set; }
        public Reservation Reservation { get; set; }

        
        private ObservableCollection<Show> shows;

        public ReservationViaClient(Reservation reservation)
        {
            InitializeComponent();
            if (DesignerProperties.GetIsInDesignMode(this))
                return;
            DataContext = this;
            Shows = new ObservableCollection<Show>(App.Model.Show);
            ClearFilter = new RelayCommand(() => { Filter = ""; });
            delete = new RelayCommand(deleteAction,IsVendor);
            Save = new RelayCommand(SaveAction, CanSaveOrCancelAction);
            Cancel = new RelayCommand(CancelAction, CanSaveOrCancelAction);
            Client = reservation.Client;
            Show = reservation.Show;
            nbrplace = reservation.Nbr;
            nomcat = reservation.NumCat - 1;

        }
        private bool IsVendor() {
            return vendor.Equals(App.CurrentUser);
        }

        public ReservationViaClient(Client client)
        {
            InitializeComponent();
            if (DesignerProperties.GetIsInDesignMode(this))
                return;
            DataContext = this;
            Shows = new ObservableCollection<Show>(App.Model.Show);
            ClearFilter = new RelayCommand(() => { Filter = ""; });
            delete = new RelayCommand(deleteAction,IsVendor);
            Save = new RelayCommand(SaveAction, CanSaveOrCancelAction);
            Cancel = new RelayCommand(CancelAction, CanSaveOrCancelAction);
            Client = client;
        }
        public int  cat;
        public int nomcat
        {
            get
            {
                return cat ;
            }
            set
            {
                cat = value;
                nbrplace = Tools.getNbrPlacesReservation(Show, Client, value + 1);
                RaisePropertyChanged(nameof(nomcat));
                RaisePropertyChanged(nameof(NbrPlaceRestante));
                RaisePropertyChanged(nameof(PlaceTotale));
                
            }
        }
        public int nbr;
        public int nbrplace
        {
            get
            {
                return nbr; ; 
            }
            set
            {

                nbr = value;
                RaisePropertyChanged(nameof(nbrplace));
                RaisePropertyChanged(nameof(NbrPlaceRestante));

            }
        }

        public ObservableCollection<Show> Shows
        {
            get
            {
                return shows;
            }
            set
            {
                shows = value;
                RaisePropertyChanged(nameof(Shows));
            }
        }

        private string filter;
        public string Filter
        {
            get { return filter; }
            set
            {
                filter = value;
                ApplyFilterAction();
                RaisePropertyChanged(nameof(Filter));
            }
        }
        private void ApplyFilterAction()
        {
            IEnumerable<Show> query = App.Model.Show;
            if (!string.IsNullOrEmpty(Filter))
                query = from m in App.Model.Show
                        where
                            m.ShowName.Contains(Filter)
                        select m;
            Shows = new ObservableCollection<Show>(query);
        }
       

        private void SaveAction()
        {
            Tools.setNbrPlacesReservation(Show, Client, nomcat + 1, nbrplace);
            App.Model.SaveChanges();
            RaisePropertyChanged(nameof(NbrPlaceRestante));
            App.Messenger.NotifyColleagues(App.MSG_RESERVATION_CHANGED, Reservation);
                
        }
        private bool CanSaveOrCancelAction()
        {
            if(IsVendor())
              return Show != null  && nbrplace>0 && nbrplace<=NbrPlaceRestante;
            return false;
        }

       private void CancelAction()
        {
            nbrplace = Tools.getNbrPlacesReservation(Show, Client, nomcat + 1);
        }
        private void deleteAction() {
            Tools.deleteReservation(Show, Client, nomcat+1);
            App.Model.SaveChanges();
            nbrplace = Tools.getNbrPlacesReservation(Show, Client, nomcat + 1);
            RaisePropertyChanged(nameof(NbrPlaceRestante));
        }
       
        public Show ShowSelection
        {
            get { return Show; }
            set
            {                 
                Show = value;
                nbrplace = Tools.getNbrPlacesReservation(Show, Client, nomcat + 1);
                RaisePropertyChanged(nameof(ShowSelection));
                RaisePropertyChanged(nameof(NbrPlaceRestante));
            }

        }

        public string ClientName
        {
            get
            {
                if (Client != null)
                    return "Reservation for :  "+Client.ClientFName +" "+ Client.ClientLName;
                return "";
            }
            set { }
        }
      
        public int NbrPlaceRestante
        {
            get
            {  
                return placeRestante();
            }
            set { RaisePropertyChanged(nameof(NbrPlaceRestante)); }
        }
        public int placeRestante()
        {
            if (Show != null)
            {
                IEnumerable<int> q = from m in App.Model.Reservation
                        where m.NumCat == (nomcat + 1) && m.NumS == Show.IdS
                        select m.Nbr;
                if (q == null)
                    return nbrTotal();
                else
                    return nbrTotal() - q.Sum();
            }
            else return nbrTotal();
        }


            public int nbrTotal() {
            if (nomcat == 0)
                return 50;
            else if (nomcat == 1)
                return 100;
            else return 200;
        }
        public int PlaceTotale
        {
            get { return nbrTotal(); }
            set { RaisePropertyChanged(nameof(PlaceTotale)); }
        }
    }
}


