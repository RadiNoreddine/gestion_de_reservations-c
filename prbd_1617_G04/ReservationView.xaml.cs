﻿using PRBD_Framework;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace prbd_1617_G04
{
    /// <summary>
    /// Logique d'interaction pour ReservationView.xaml
    /// </summary>
    public partial class ReservationView : UserControlBase
    {
        private static User vendor = App.Model.User.Find(2);
        private ObservableCollection<Reservation> reservations;
    
        public ObservableCollection<Reservation> Reservations
        {
            get
            {
                return reservations;
            }
            set
            {
                reservations = value;
                RaisePropertyChanged(nameof(Reservations));
            }
        }

        private string filter;
        public string Filter
        {
            get { return filter; }
            set
            {
                filter = value;
                ApplyFilterAction();
                RaisePropertyChanged(nameof(Filter));
            }
        }
        public ICommand ClearFilter { get; set; }
        public ICommand DeleteReservation { get; set; }
        public Reservation reservation { get; set; }
        public ICommand DisplayReservationDetails { get; set; }

        public Reservation ReservationSelection {
            get { return reservation; }
            set { reservation = value; RaisePropertyChanged(nameof(ReservationSelection)); }
        }

        public ReservationView()
        {
            InitializeComponent();
            if (DesignerProperties.GetIsInDesignMode(this))
                return;

            DataContext = this;
            Reservations = new ObservableCollection<Reservation>(App.Model.Reservation);
            ClearFilter = new RelayCommand(() => { Filter = ""; });
            DeleteReservation = new RelayCommand( remove, visible);
            App.Messenger.Register<Reservation>(App.MSG_RESERVATION_CHANGED, reservation => { ApplyFilterAction(); });
            DisplayReservationDetails = new RelayCommand<Reservation>(m => { App.Messenger.NotifyColleagues(App.MSG_DISPLAY_RESERVATION, m); });
        }
        public bool visible()
        {
           return reservation!=null && vendor.Equals(App.CurrentUser) ;
        }
        private void remove()
        {
                Tools.deleteReservation(reservation.Show, reservation.Client, reservation.Category.IdCat);
                App.Model.SaveChanges();
                App.Messenger.NotifyColleagues(App.MSG_RESERVATION_CHANGED, reservation);
           
        }

        private void ApplyFilterAction()
        {
            IEnumerable<Reservation> query = App.Model.Reservation;
            if (!string.IsNullOrEmpty(Filter))
                query = from m in App.Model.Reservation
                        where
                            m.Client.ClientFName.Contains(Filter)|| m.Client.ClientLName.Contains(Filter)|| m.Show.ShowName.Contains(Filter)
                        select m;
            Reservations = new ObservableCollection<Reservation>(query);

        }
       

    }
}
