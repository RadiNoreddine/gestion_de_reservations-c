﻿using PRBD_Framework;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace prbd_1617_G04
{
    /// <summary>
    /// Logique d'interaction pour ClientView.xaml
    /// </summary>
    public partial class ClientView : UserControlBase
    {
        public static User vendor = App.Model.User.Find(2);
        public Client Client { get; set; }
        public bool selectionned;
        public ICommand Save { get; set; }
        public ICommand Cancel { get; set; }
        public ICommand Delete { get; set; }
        public ICommand ClearFilter { get; set; }
        public ICommand NewClient { get; set; }
        public ICommand NewReservation { get; set;  }
        public ICommand DisplayClientDetails { get; set; }
        private ObservableCollection<Client> clients;
        public ObservableCollection<Client> Clients
        {
            get
            {
                return clients;
            }
            set
            {
                clients = value;
                RaisePropertyChanged(nameof(Clients));
            }
        }

        private string filter;
        public string Filter
        {
            get { return filter; }
            set
            {
                filter = value;
                ApplyFilterAction();
                RaisePropertyChanged(nameof(Filter));
            }
        }

     
        public ClientView()
        {
            InitializeComponent();
            if (DesignerProperties.GetIsInDesignMode(this))
                return;
            DataContext = this;
            Save = new RelayCommand(SaveAction, CanSaveOrCancelAction);
            Cancel = new RelayCommand(CancelAction, CanSaveOrCancelAction);
            Delete = new RelayCommand(DeleteAction, () => { return IsExisting && isVendor(); });
            Clients = new ObservableCollection<Client>(App.Model.Client);
            ClearFilter = new RelayCommand(() => { Filter = ""; });
            NewClient = new RelayCommand(CreateNewClient, isVendor);
            App.Messenger.Register<Client>(App.MSG_CLIENT_CHANGED, client => { ApplyFilterAction(); });
            DisplayClientDetails = new RelayCommand<Client>(m => { App.Messenger.NotifyColleagues(App.MSG_RESERVATION_CLIENT, m); });
            CreateNewClient();
        }
        private void ApplyFilterAction()
        {
            IEnumerable<Client> query = App.Model.Client;
            if (!string.IsNullOrEmpty(Filter))
                query = from m in App.Model.Client
                        where
                            m.ClientLName.Contains(Filter) || m.ClientFName.Contains(Filter)
                        select m;
            Clients = new ObservableCollection<Client>(query);
        }
     
        public void CreateNewClient()
        {
            Client = new Client();
            ClientFName = null;
            ClientLName = null;
            Bdd = DateTime.Today;
            PostalCode = null;
            IsNew = true;
            selectionned = false;
        }
        private bool isVendor() {
            return vendor.Equals(App.CurrentUser);
        }
        public Client ClientSelection
        {
            get { return Client; }
            set
            {
                if (value != null)
                {
                    Client = value;
                    IsNew = false;
                    selectionned = true;
                    ClearErrors();
                    RaisePropertyChanged(nameof(ClientSelection));
                    ClientFName = Client.ClientFName;
                    ClientLName = Client.ClientLName;
                    PostalCode = Client.PostalCode;
                    Bdd = Client.Bdd;
                    
                }
                return;
            }
        }

        private bool isNew;
        public bool IsNew
        {
            get { return isNew; }
            set
            {
                isNew = value;
                RaisePropertyChanged(nameof(IsNew));
            }
        }

        public string ClientFName
        {
            get {
                return Client.ClientFName; }
            set
            {
                Client.ClientFName = value;
                App.Messenger.NotifyColleagues(App.MSG_TITRE_CHANGED, string.IsNullOrEmpty(value) ? "<new Client>" : value);
                RaisePropertyChanged(nameof(ClientFName));
                ValidateClient();
            }
        }

        public string ClientLName
        {
            get {
               
                return Client.ClientLName; }
            set
            {
               
                Client.ClientLName = value;
                RaisePropertyChanged(nameof(ClientLName));
                ValidateClient();
            }
        }

        public DateTime? Bdd
        {
            get
            {
                return Client.Bdd; }
            set
            {
                Client.Bdd = value;
                RaisePropertyChanged(nameof(Bdd));
            }
        }
        public int? PostalCode
        {
            get {
                
                return Client.PostalCode; }
            set
            {
               
                Client.PostalCode =value;
                RaisePropertyChanged(nameof(PostalCode));
                ValidateClient();
            }
        }
        public bool IsExisting { get { return selectionned; } }

        private void SaveAction()
        {
            if (IsNew)
            {
                
                App.Model.Client.Add(Client);
                IsNew = false;
            }
            App.Model.SaveChanges();
           App.Messenger.NotifyColleagues(App.MSG_CLIENT_CHANGED, Client);
        }

        private bool CanSaveOrCancelAction()
        {
            if (isVendor())
            {
                if (IsNew)
                    return !string.IsNullOrEmpty(ClientFName) && !HasErrors;

                var change = (from c in App.Model.ChangeTracker.Entries<Client>()
                              where c.Entity == Client
                              select c).FirstOrDefault();
                return change != null && change.State != EntityState.Unchanged && !HasErrors;
            }
            return false;
        }
        private void CancelAction()
        {
            if (IsNew)
            {
                ClientFName = null;
                ClientLName = null;
                Bdd = null;
                PostalCode = null;
                RaisePropertyChanged(nameof(Client));
            }
            else
            {
                var change = (from c in App.Model.ChangeTracker.Entries<Client>()
                              where c.Entity == Client
                              select c).FirstOrDefault();
                if (change != null)
                {
                    change.Reload();
                    RaisePropertyChanged(nameof(ClientLName));
                    RaisePropertyChanged(nameof(ClientFName));
                    RaisePropertyChanged(nameof(Bdd));
                    RaisePropertyChanged(nameof(PostalCode));
                }
            }
        }
        private void DeleteAction()
        {
            var q = from m in App.Model.Client
                    where m.IdC == 8
                    select m;
            if (q.FirstOrDefault() != null)
                App.Model.Client.Remove(q.FirstOrDefault());
            App.Model.Client.Remove(Client);
            App.Model.SaveChanges();
            App.Messenger.NotifyColleagues(App.MSG_CLIENT_CHANGED, Client);
            CreateNewClient();
            
        }
       
        private void ValidateClient()
        {
            ClearErrors();

            string a = Convert.ToString(PostalCode);
            var c = App.Model.Client.Find(getId(ClientFName, ClientLName));
            if (string.IsNullOrEmpty(a))
                AddError("PostalCode", "le champ est obligatoire !");
            if (PostalCode != null)
            { 
                if (a.Length <= 3)
                    AddError("PostalCode", "au minimume 4 nombre !");
            }
            RaiseErrors();
       
            if (string.IsNullOrEmpty(ClientFName))
           
                AddError("ClientFName", "le champ est obligatoire !");

            if (string.IsNullOrEmpty(ClientLName))

                AddError("ClientLName", "le champ est obligatoire !");

            RaiseErrors();
            if (c != null) {
                AddError("ClientFName", "le client existe deja !");
            }
        }

        private int getId(string fname, string lname) {

            var q = from m in App.Model.Client
                    where m.ClientFName == fname && m.ClientLName == lname
                    select m.IdC;
                   return q.FirstOrDefault();
        }




    }
}
