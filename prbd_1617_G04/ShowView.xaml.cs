﻿using Microsoft.Win32;
using PRBD_Framework;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data.Entity;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace prbd_1617_G04
{
    /// <summary>
    /// Logique d'interaction pour AdminView.xaml
    /// </summary>
    public partial class ShowView : UserControlBase
    {
        private static User admin= App.Model.User.Find(1);
        private bool selectionned;
        private bool isNew = true;
        public bool IsNew
        {
            get { return isNew; }
            set
            {
                isNew = value;
                RaisePropertyChanged(nameof(IsNew));
            }
        }
        public bool IsExisting { get { return selectionned; } }

        private ObservableCollection<Show> shows;
        public ObservableCollection<Show> Shows
        {
            get
            {
                return shows;
            }
            set
            {
                shows = value;
                RaisePropertyChanged(nameof(Shows));
            }
        }

        private string filter;
        public string Filter
        {
            get { return filter; }
            set
            {
                filter = value;
                ApplyFilterAction();
                RaisePropertyChanged(nameof(Filter));
            }
        }

        public ICommand Save { get; set; }
        public ICommand Cancel { get; set; }
        public ICommand Delete { get; set; }
        public ICommand LoadImage { get; set; }
        public ICommand ClearImage { get; set; }
        public ICommand ClearFilter { get; set; }
        public ICommand DisplayShowDetails { get; set; }
        public ICommand NewShow { get; set; }
        public Show Show { get; set; }
       
        public ShowView()
        {
            InitializeComponent();
            if (DesignerProperties.GetIsInDesignMode(this))
                return;
            DataContext = this;
            Shows = new ObservableCollection<Show>(App.Model.Show);
            NewShow = new RelayCommand(CreatNewShow , isAdmin);
            ClearFilter = new RelayCommand(() => { Filter = ""; });
            App.Messenger.Register<Show>(App.MSG_SHOW_CHANGED, show => { ApplyFilterAction(); });
            DisplayShowDetails = new RelayCommand<Show>(m => {App.Messenger.NotifyColleagues(App.MSG_RESERVATION_SHOW, m); });
            Save = new RelayCommand(SaveAction, CanSaveOrCancelAction);
            Cancel = new RelayCommand(CancelAction, CanSaveOrCancelAction);
            Delete = new RelayCommand(DeleteAction, () => { return IsExisting && isAdmin(); });
            LoadImage = new RelayCommand(LoadImageAction,isAdmin);
            ClearImage = new RelayCommand(ClearImageAction,isAdmin);
            inisialiseVue();
        }
        public void CreatNewShow() {
            inisialiseVue(); 
        }
        private bool isAdmin() {
            return admin.Equals(App.CurrentUser);
        }
        public Show ShowSelection
        {
            get { return Show; }
            set
            {
                if (value != null)
                {
                    Show = value;
                    IsNew = false;
                    selectionned = true;
                    ClearErrors();
                    RaisePropertyChanged(nameof(ShowSelection));
                    ShowName = Show.ShowName;
                    Description = Show.Description;
                    Date = Show.ShowDate;
                    Poster = Show.Poster;
                    getPrice();
                    getPlaceRestantes();
                }
                return;
            }
        }

        private void ApplyFilterAction()
        {
            IEnumerable<Show> query = App.Model.Show;
            if (!string.IsNullOrEmpty(Filter))
                query = from m in App.Model.Show
                        where
                            m.ShowName.Contains(Filter)
                        select m;
            Shows = new ObservableCollection<Show>(query);
        }
        public string ShowName
        {
            get {
               
                return Show.ShowName; }
            set
            {
               
                this.Show.ShowName = value;
                RaisePropertyChanged(nameof(ShowName));
                App.Messenger.NotifyColleagues(App.MSG_TITRE_CHANGED, string.IsNullOrEmpty(value) ? "<new Show>" : value);
                validateShow();
            }
        }
        public string Description
        {
            get { 
               
             return Show.Description; }
            
            set
            {
                this.Show.Description = value;
                RaisePropertyChanged(nameof(Description));
                validateShow();
            }
          
        }
        public DateTime Date
        {
            get
            {
                return Show.ShowDate;
            }
            set
            {
                this.Show.ShowDate = value;
                RaisePropertyChanged(nameof(Date));
            }
        }

        public byte[] Poster
        {
            get
            {   
                 return Show.Poster;
            }
            set
            {
                Show.Poster = value;
                RaisePropertyChanged(nameof(Poster));
            }
        }
        private decimal catA;
        public decimal CatA
        {
            get { return catA; }
            set
            {
                catA = value;
                RaisePropertyChanged(nameof(CatA));
            }

        }
        private decimal catB;
        public decimal CatB
        {
            get { return catB; }
            set
            {
                catB=value;
                RaisePropertyChanged(nameof(CatB));
            }

        }
        private decimal catC;
        public decimal CatC
        {
            get { return catC; }
            set
            {
                catC = value;
                RaisePropertyChanged(nameof(CatC));
                
            }
        }
        private void addPrice()
        {
            if(CatA>=0)
            Tools.setPrice(Show, "CatA", CatA);
            if(CatB>=0)
            Tools.setPrice(Show, "CatB", CatB);
            if(CatC>=0)
            Tools.setPrice(Show, "CatC", CatC);
        }
        private void getPrice()
        {
            CatA = Tools.getPrice(Show, "CatA");
            CatB = Tools.getPrice(Show, "CatB");
            CatC = Tools.getPrice(Show, "CatC");
        }
        private void getPlaceRestantes()
        {
            RestCatA.Text = Tools.getPlaceRest("CatA", Show);
            RestCatB.Text = Tools.getPlaceRest("CatB", Show);
            RestCatC.Text = Tools.getPlaceRest("CatC", Show);
        }
        private void SaveAction()
        {
            if (IsNew)
            {
                
                App.Model.Show.Add(Show);
                IsNew = false;
            }
            addPrice();
            App.Model.SaveChanges();
            App.Messenger.NotifyColleagues(App.MSG_SHOW_CHANGED, Show);

        }

        private bool CanSaveOrCancelAction()
        {
            if (isAdmin())
            {
                if (IsNew)
                    return !string.IsNullOrEmpty(ShowName) && !HasErrors;

                var change = (from c in App.Model.ChangeTracker.Entries<Show>()
                              where c.Entity == Show
                              select c).FirstOrDefault();

                return (change != null && change.State != EntityState.Unchanged);
            }
            return false;
        }

        private void LoadImageAction()
        {
            var fd = new OpenFileDialog();
            if (fd.ShowDialog() == true)
            {
                var filename = fd.FileName;
                if (filename != null && File.Exists(filename))
                {
                    var img = System.Drawing.Image.FromFile(filename);
                    var ms = new MemoryStream();
                    var ext = System.IO.Path.GetExtension(filename).ToUpper();
                    switch (ext)
                    {
                        case ".PNG":
                            img.Save(ms, ImageFormat.Png);
                            break;
                        case ".GIF":
                            img.Save(ms, ImageFormat.Gif);
                            break;
                        case ".JPG":
                            img.Save(ms, ImageFormat.Jpeg);
                            break;
                    }
                    Poster = ms.ToArray();
                    RaisePropertyChanged(nameof(Poster));
                }
            }
        }
        private void inisialiseVue() {
            IsNew = true;
            selectionned = false;
            Show = new Show();
            ShowName = null;
            Description = null;
            Poster = null;
            Date = DateTime.Today;
            getPrice();
            getPlaceRestantes();


        }
        private void CancelAction()
        {
            if (IsNew)
            {
                inisialiseVue();
                RaisePropertyChanged(nameof(Show));
            }
            else
            {
                var change = (from c in App.Model.ChangeTracker.Entries<Show>()
                              where c.Entity == Show
                              select c).FirstOrDefault();
                if (change != null)
                {
                    change.Reload();
                    RaisePropertyChanged(nameof(ShowName));
                    RaisePropertyChanged(nameof(Description));
                    RaisePropertyChanged(nameof(Date));
                    RaisePropertyChanged(nameof(Poster));
                    getPrice();
                }
            }
        }
        private void validateShow() {
            ClearErrors();
         
           
            var c = App.Model.Client.Find(getId(ShowName, Description,Date));
            if (string.IsNullOrEmpty(ShowName))
                AddError("ShowName", "le champ est obligatoire !");

            if (string.IsNullOrEmpty(Description))

                AddError("Description", "le champ est obligatoire !");
            if (CatA <0)
                AddError("CatA", " la valeur doit > 0 !");
            if ( CatB < 0)
                AddError("CatB", " la valeur doit > 0 !");
            if ( CatC < 0)
                AddError("CatC", "la valeur doit > 0 !");

            if (c != null)
            {
                AddError("ClientFName", "le Show existe deja !");
            }
        }
        private int getId(string a, string b, DateTime? d)
        {
            var q = from m in App.Model.Show
                    where m.ShowName == a && m.Description == b && m.ShowDate == d
                    select m.IdS;
            return q.FirstOrDefault();


        }
        private void DeleteAction()
        {
                App.Model.Show.Remove(Show);
                App.Model.SaveChanges();
                App.Messenger.NotifyColleagues(App.MSG_SHOW_CHANGED, Show);
                inisialiseVue();
        }
        private void ClearImageAction()
        {
            Show.Poster = null;
            RaisePropertyChanged(nameof(Poster));
        }

    }

}
